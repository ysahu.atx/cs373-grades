#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html


# Counts the total number of points earned in a specific category, which is
# calculated by finding the number of Ms and Es, as well as accounting for
# the "2 Es offset 1 R" rule
def count_total(scores: list[int]) -> int:
    counts = [0] * 4
    for score in scores:
        assert 0 <= score <= 3
        counts[score] += 1
    return counts[3] + counts[2] + min(counts[1], counts[3] // 2)


# Checks whether or not a list of points is above or at the threshold to
# receive a certain grade
def above_threshold(counts: list[int], threshold: list[int]) -> bool:
    for i in range(5):
        if counts[i] < threshold[i]:
            return False
    return True


# Stores a list representing letter grades, as well as a matrix representing
# the thresholds needed to achieve those grades
grade_list = ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-"]
threshold_counts = [
    [5, 11, 13, 13, 39],
    [5, 11, 13, 13, 38],
    [4, 10, 12, 12, 37],
    [4, 10, 12, 12, 35],
    [4, 10, 11, 11, 34],
    [4, 9, 11, 11, 32],
    [4, 9, 10, 10, 31],
    [4, 8, 10, 10, 29],
    [3, 8, 9, 9, 28],
    [3, 8, 9, 9, 27],
    [3, 7, 8, 8, 25],
]

# -----------
# grades_eval
# -----------


# This function works by first calculating the total number of points in each
# category. Then, it goes through each possible threshold associated with each
# letter grade (in descending order), and checks if the threshold is met. If
# it is met for a certain grade, then it returns that grade as the answer. If
# no threshold is met, it returns "F". This approach allows for flexibility,
# since the thresholds and letter grades can be changed easily.
def grades_eval(l_l_scores: list[list[int]]) -> str:
    # Checks assertions and determines the point counts for each category
    assert l_l_scores
    assert [len(scores) for scores in l_l_scores] == [5, 12, 14, 14, 42]
    counts = [count_total(scores) for scores in l_l_scores]

    # Loops through each letter grade and checks if the threshold is met
    for i in range(11):
        if above_threshold(counts, threshold_counts[i]):
            return grade_list[i]

    # Returns "F" if no thresholds were met
    return "F"

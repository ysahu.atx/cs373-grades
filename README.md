# CS373: Software Engineering Grades Repo

* Name: Yuvraj Sahu

* EID: ys23486

* GitLab ID: ysahu.atx

* HackerRank ID: ysahu_atx

* Git SHA: 88e7c091e704c5fe401cdd127516985377de33ef

* GitLab Pipelines: https://gitlab.com/ysahu.atx/cs373-grades/-/pipelines/1160050839

* Estimated completion time: 8

* Actual completion time: 7

* Comments: 

The above pipeline link is the most recent pipeline (as of the commit a33135f5). To get all pipelines, the following link should work: https://gitlab.com/ysahu.atx/cs373-grades/-/pipelines.

I was surprised (and pleased) to find out that my total completion time was very slightly under my estimated completion time! This did show me that it is sometimes beneficial to slightly overestimate, since unexpected delays can happen.

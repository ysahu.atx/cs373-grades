#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# --------------
# test_Grades.py
# --------------

# -------
# imports
# -------

import unittest  # main, TestCase

import Grades

# ----------------
# test_grades_eval
# ----------------


class test_grades_eval(unittest.TestCase):
    # Commented this test case (since it does not contain all grades)
    # This test is fixed in test_1
    # def test_0 (self) -> None :
    #     l_l_scores: list[list[int]] = [[0, 0, 0, 0, 0]]
    #     letter:     str             = Grades.grades_eval(l_l_scores)
    #     self.assertEqual(letter, "B-")

    def test_1(self) -> None:
        l_l_scores: list[list[int]] = [[0] * 5, [0] * 12, [0] * 14, [0] * 14, [0] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_2(self) -> None:
        l_l_scores: list[list[int]] = [[1] * 5, [1] * 12, [1] * 14, [1] * 14, [1] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_3(self) -> None:
        l_l_scores: list[list[int]] = [[2] * 5, [2] * 12, [2] * 14, [2] * 14, [2] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_4(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 5, [3] * 12, [3] * 14, [3] * 14, [3] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_5(self) -> None:
        l_l_scores: list[list[int]] = [
            [1, 1, 1, 2, 3],
            [3] * 12,
            [3] * 14,
            [3] * 14,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_6(self) -> None:
        l_l_scores: list[list[int]] = [
            [1, 1, 1, 3, 3],
            [1] * 5 + [2] * 7,
            [3] * 14,
            [3] * 14,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_7(self) -> None:
        l_l_scores: list[list[int]] = [
            [1, 1, 2, 2, 3],
            [1] * 4 + [2] * 8,
            [3] * 14,
            [3] * 14,
            [2] * 27 + [1] * 15,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D")

    def test_8(self) -> None:
        l_l_scores: list[list[int]] = [
            [1, 1, 1, 3, 3],
            [3] * 12,
            [3] * 14,
            [3] * 14,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D+")

    def test_9(self) -> None:
        l_l_scores: list[list[int]] = [
            [3] * 5,
            [1] * 6 + [2] * 2 + [3] * 4,
            [3] * 14,
            [3] * 14,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C-")

    def test_10(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 5,
            [2] * 12,
            [3] * 14,
            [0] * 2 + [1] * 4 + [2] * 4 + [3] * 4,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C")

    def test_11(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 5,
            [2] * 12,
            [3] * 14,
            [2] * 14,
            [0] * 10 + [3] * 32,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C+")

    def test_12(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 5,
            [3] * 3 + [1] * 1 + [2] * 8,
            [0] * 1 + [1] * 4 + [2] * 4 + [3] * 5,
            [2] * 14,
            [2] * 21 + [3] * 21,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B-")

    def test_13(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 12,
            [2] * 14,
            [2] * 13 + [1] * 1,
            [1] * 6 + [2] * 36,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B")

    def test_14(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 12,
            [3] * 14,
            [3] * 14,
            [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B+")

    def test_15(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 5,
            [2] * 11 + [1] * 1,
            [2] * 13 + [1] * 1,
            [2] * 13 + [1] * 1,
            [3] * 38 + [0] * 4,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A-")

    def test_16(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 5,
            [2] * 11 + [1] * 1,
            [2] * 13 + [1] * 1,
            [2] * 13 + [1] * 1,
            [3] * 39 + [0] * 3,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_17(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 8 + [1] * 4,
            [2] * 10 + [1] * 4,
            [2] * 10 + [1] * 4,
            [2] * 29 + [1] * 13,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C-")

    def test_18(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 9 + [1] * 3,
            [2] * 10 + [1] * 4,
            [2] * 10 + [1] * 4,
            [2] * 31 + [1] * 11,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C")

    def test_19(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 9 + [1] * 3,
            [2] * 11 + [1] * 3,
            [2] * 11 + [1] * 3,
            [2] * 32 + [1] * 10,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C+")

    def test_20(self) -> None:
        l_l_scores: list[list[int]] = [
            [2] * 4 + [1] * 1,
            [2] * 10 + [1] * 2,
            [2] * 11 + [1] * 3,
            [2] * 11 + [1] * 3,
            [2] * 34 + [1] * 8,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B-")

    # Bonus tests have been commented out below (to satisfy the 20 public
    # methods rule from pylint)

    #     def test_21 (self) -> None :
    #     l_l_scores: list[list[int]] = [[2] * 4 + [1] * 1, [2] * 10 + [1] * 2,
    #                                    [2] * 12 + [1] * 2, [2] * 12 + [1] * 2,
    #                                    [2] * 35 + [1] * 7]
    #     letter:     str             = Grades.grades_eval(l_l_scores)
    #     self.assertEqual(letter, "B")

    # def test_22 (self) -> None :
    #     l_l_scores: list[list[int]] = [[2] * 4 + [1] * 1, [2] * 10 + [1] * 2,
    #                                    [2] * 12 + [1] * 2, [2] * 12 + [1] * 2,
    #                                    [2] * 37 + [1] * 5]
    #     letter:     str             = Grades.grades_eval(l_l_scores)
    #     self.assertEqual(letter, "B+")

    # def test_23 (self) -> None :
    #     l_l_scores: list[list[int]] = [[2] * 5, [2] * 11 + [1] * 1,
    #                                    [2] * 13 + [1] * 1, [2] * 13 + [1] * 1,
    #                                    [2] * 38 + [1] * 4]
    #     letter:     str             = Grades.grades_eval(l_l_scores)
    #     self.assertEqual(letter, "A-")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
